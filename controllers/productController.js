const Product = require("../models/productModel");
const bcrypt = require("bcrypt");
const User = require("../models/userModel")
const {createAccessToken, verify, decode} = require("../middleware/auth")

module.exports.getItems = async (body) =>{ // find all active products
	const product = await Product.find({isActive: true});
	return product
}


module.exports.renderProducts = async () =>{ // find all active products
	const products = await Product.find({ isActive: true })
	return products
}

module.exports.findInActive = async (body) =>{ // find all active products
	const product = await Product.find({isActive: false});
	return product
}

module.exports.toggleProduct = async(params) =>{ // toggle active product true or false
  const  { productId } = params
  const productData = await Product.findById(productId)
 

  let boolean = productData.isActive
  if(boolean === true){
    var toggle = false
  }else{
    var toggle = true
  }
   console.log(boolean)  
 return await Product.findByIdAndUpdate(productId, {isActive: toggle, new: true}).then((result,error)=>{
      if(error){
        return error
      }else{
        return `isActive: ${toggle}`
      }
  })
  
} 																

module.exports.findProduct = async (params) =>{ //find specific product
	const product = await Product.findById(params.productId);
	return product
}


module.exports.createProduct = async (body, user) =>{ //create product admin and seller
	const {name, description, price, quantity, category, image} = body;
	const userData = await User.findById(user.id).populate("products").exec()
	console.log(userData)
	const newProduct = new Product({
		name,
		description,
		price,
		quantity,
		category,
		image
	})
	
	 newProduct.save().then((result, error) =>{
		if(error){
			return false
		}else{
			return true
		}
	})
	 console.log(userData)
	if (userData.isSeller){
		userData.sellerProducts.push(newProduct);
		return await userData.save().then((res, error)=>{
			if(error){
				return false
			}else{
				return userData.products
			}
		})
	}	

}

module.exports.editProduct = async (params, body, user) => { // admin edit products
	const { name, description, price } = body
	const paramId = params.productId
	const userData = await User.findById(user) ;
	const sellerProducts = userData.sellerProducts

	const product = await Product.findByIdAndUpdate(params.productId,{
		name,
		description,
		price
	},{runValidators:true, new:true}
	)

	if(sellerProducts.includes(paramId) || userData.isAdmin){
		product.save().then((data, error) =>{
			if(error){
				return false
			}else{
				return true
			}
		})
	}else{
		return "Error: Auth Failed - you don't have a privilege for this action"
	}
}

module.exports.archiveProduct = async(params, body)=>{
	const { productId } = params;
	const productData = await Product.findById(productId);

	let boolean = productData.isActive;
  if (boolean === true) {
    var toggle = false;
  } else {
    var toggle = true;
  }
  console.log(boolean)
	return await Product.findByIdAndUpdate( productId, { isActive: toggle },{ new:true })
	.then((data, error) =>{
		if(error){
			return false
		}else{
			return `The product is now Active: ${toggle}`	
		}
	})
}