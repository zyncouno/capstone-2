const express = require("express");
const User = require("../models/userModel");
const bcrypt = require("bcrypt");
const {createAccessToken, verify, decode } = require("../middleware/auth");

const errorHandler = (err) =>{
  console.log(err.message, err.code);
  let errors = { firstName: "", lastName: "", email: "",cellphoneNum: "", password: "" };

  if(err.message === "incorrect email"){ //login email error
    errors.email = "That Email is not yet registered"
  }

  if(err.message === "incorrect password"){ //password email error
    errors.password = "The password you've provided is incorrect"
  }
  
  if(err.code === 11000){
    errors.email = "That email is already registered"
    return errors;
  }
  if (err.message.includes("User validation failed")) {
      
      Object.values(err.errors).forEach(({properties}) => {
   
      errors[properties.path] = properties.message;
    });
  }
  return errors;
}

//Create user
module.exports.createUser = async(req, res) =>{
  const {firstName, lastName, email, cellphoneNum, password} = req.body

  try{  
    const user = await User.create({
        firstName,
        lastName,
        email,
        cellphoneNum,
        password: bcrypt.hashSync(req.body.password, 10)
      })
    const userData = await User.findOne({ email }).exec();
    const token = createAccessToken(userData);
    res.cookie("jwt", token, { httpOnly: true })
    res.status(201).json({user: user._id });
  }
  catch(err){
    const errors = errorHandler(err);
    res.status(400).json({ errors });
  }
}

//Login user
module.exports.userLogin = async(req, res) => {
  const { email, password } = req.body
  const user = await User.findOne({ email });

  try{
    if(user){
      const isPasswordCorrect = bcrypt.compareSync(
        password,
        user.password
      );
      
      if(isPasswordCorrect){
        const token = createAccessToken(user) 
        res.cookie("jwt", token, { httpOnly: true })
        res.status(201).json({user: user._id});
        
      }else{
        throw Error(`incorrect password`)
      }   
    }else{
      throw Error("incorrect email")
    }
  }
  catch(err){
    const errors = errorHandler(err);
    res.status(400).json({ errors });
    }
}

//Logout user
module.exports.userLogout = async(req, res) =>{
  res.cookie("jwt","",{ maxAge: 1 });
  res.redirect("/api/home")
}

//Update User
module.exports.UpdateUser = (body) => {
	const {firstName, lastName, email, cellphoneNum, password} = body;

    return User.findOneAndUpdate({ email }, 
    	{firstName, lastName, cellphoneNum},
    	{runValidators: true},  	
     )
    .then((result) =>{
	    if(result === null){
	   	   return `The email you've provided doesn't exist: You should register instead`;	
	   		
	   	 }else{
	   	 	 return 'User update Success';
	   	 }	
    })
    .catch((err) =>{
    	return err;
    })
}
//Check Profile

module.exports.checkProfile = async(userProfile) =>{
  const profile = await User.findById(userProfile);
    profile.password = undefined;
    return profile;
};

//Set Admin
module.exports.setUserAdmin = async(params) =>{
  const {userId} = params;
  const userData = await User.findById(userId);

  let boolean = userData.isAdmin;
  if(boolean === true){
    var toggle = false;
  }else{
    var toggle = true;
  }
    
 return await User.findByIdAndUpdate(userId, {isAdmin: toggle, new: true}).then((result,error)=>{
      if(error){
        return error;
      }else{
        return `isAdmin: ${toggle}`;
      }
  })
  
};

