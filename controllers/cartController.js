const express = require("express");
const Cart = require("../models/cartModel");
const Product = require("../models/productModel")
const User = require("../models/userModel")

module.exports.getAllUserCartItems = async (user) =>{ //Show User Cart Items
	 const userData = await User.findById(user).populate("cart")
	 return userData.cart
}

module.exports.addToCart = async (params, user) =>{
	const userData = await User.findById(user);
	const product = await Product.findById(params.productId);
	try{
		userData.cart.push(product);
		return await userData.save().then((res, error)=>{
		if(error){
			return false
		}else{
			return userData.cart
		}
	})
	}catch(err){
		return err
	}	
}


module.exports.getAllUserWishlist = async (user) =>{ //Show User Wishlist
	const userData = await User.findById(user).populate("wishlist").exec()
	return userData.wishlist
}


module.exports.addToWishlist = async (params, body, user) =>{ // Add a product to wishlist
	const product = await Product.findById(params.productId);
	const userData = await User.findById(user).populate("wishlist").exec();;
	
	userData.wishlist.push(product);
	return await userData.save().then((res, error)=>{
		if(error){
			return false
		}else{
			return userData.wishlist
		}
	})
}

