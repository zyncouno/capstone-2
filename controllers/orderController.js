const express = require("express");
const Order = require("../models/orderModel");
const User = require("../models/userModel")
const Cart = require("../models/cartModel")




module.exports.adminShowAllOrders = async (req, res) =>{ // show all orders from the data base
	const order = await Order.find({}).populate("orders").exec();
	return order
}



module.exports.getAllUserOrders = (user) =>{ // show orders of the current loggedin user
	return User.findById(user).populate("orders").then((result) =>{
		return result.orders
	})
}



module.exports.addToOrder = async (body, user) =>{ // add order
	const userData = await User.findById(user).populate("orders");
	const cartData = await Cart.find({ placedBy: user})
	
	let total = 0
	for (let i = 0; i < cartData.length; i++){
		 total += cartData[i].cartTotal
	}

	const orderInfo = new Order({
		items: cartData,
		paymentIntent: body.paymentIntent,
		total: total,
		orderedBy: user
	})

	orderInfo.save().then((result, error) =>{
		if(error){
			return false
		}else{
			return console.log(userData.orders)
		}
	})
	
	userData.orders.push(orderInfo);
	return await userData.save().then((res, error)=>{
		if(error){
			return false
		}else{
			return orderInfo
		}
	})	
}

 module.exports.changeStatus = async (body,  params)=>{ //order status update
 	const { orderId } = params;
 	return await Order.findByIdAndUpdate(orderId, { orderStatus: body.orderStatus },{runValidators:true, new:true})
 	.then( (result, error)=>{
 		if(error){
 			return error
 		}else{
 			return true
 		}
 	}) 
 }