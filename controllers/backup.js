module.exports.createOrUser = (body) => {

    return User.findOne({ email: body.email })
    .then((result) =>{
	    if(result === null){
	   	   	const newUser = new User({
		   		firstName: body.firstName,
		   		lastName: body.lastName,
		   		email: body.email,
		   		cellphoneNum: body.cellphoneNum,
	   		})
	   		return newUser.save()
	   		.then(()=>{
	   			return true;
	   		})
	   		.catch((err) =>{
	   			return false
	   			console.log(err)
	   		})
	   	 }else{
	   	 	return false
	   	 }	
    })
}

module.exports.addToOrder = async (body, user) =>{
	const userData = await User.findById(user).populate("orders");
	const cartData = await Cart.find({ placedBy: user})
	
	let total = 0
	for (let i = 0; i < cartData.length; i++){
		 total += cartData[i].cartTotal
	}

	const orderInfo = new Order({
		items: cartData,
		paymentIntent: body.paymentIntent,
		total: total,
		orderedBy: user
	})

	orderInfo.save().then((result, error) =>{
		if(error){
			return false
		}else{
			return console.log(userData.orders)
		}
	})
	
	userData.orders.push(orderInfo);
	return await userData.save().then((res, error)=>{
		if(error){
			return false
		}else{
			return orderInfo
		}
	})
	
}