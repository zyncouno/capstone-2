const express = require("express");
const mongoose = require("mongoose")

const userRouter = express.Router();
const userController = require("../controllers/userController")
const { createAccessToken, decodeCookiejwt, verifyCookieJWT} = require("../middleware/auth")
const { isAdmin, isSeller } = require("../middleware/middleware")
const { createUser, userLogin, userLogout } = require("../controllers/userController")


//User Sign Up
userRouter.get("/signup", (req, res)=>{ //user register view
	res.render("signup.ejs") 	
})


userRouter.post("/signup", createUser)

//User Login /logout
userRouter.get("/login", (req, res)=>{ //user login
	res.render("login.ejs") 
})

userRouter.post("/login", userLogin);

userRouter.get("/logout", userLogout);

//User Update
userRouter.post("/user/update", verifyCookieJWT,(req, res) =>{ //update user
	userController.UpdateUser(req.body).then((resFromCreateOrUpdateUser)=>{
		res.send(resFromCreateOrUpdateUser)
	})
})

//User Check Profile	
userRouter.get("/user/profile", verifyCookieJWT, (req,res) =>{
	const userProfile = decodeCookiejwt(req.cookies.jwt)
	console.log(userProfile.id)
	userController.checkProfile(userProfile.id).then((resultfromCheckProfile)=>{
	res.send(resultfromCheckProfile)
	})
})

//User toggle role	
userRouter.post("/user/:userId/setAdmin", verifyCookieJWT, isAdmin, (req,res) =>{
	const userProfile = decodeCookiejwt(req.cookies.jwt)
	userController.setUserAdmin(req.params).then((resultfromSetAdmin)=>{
	res.send(resultfromSetAdmin)
	})
})



module.exports = userRouter;  