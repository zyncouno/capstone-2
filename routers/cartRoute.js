const express = require("express");
const mongoose = require("mongoose")
const cartRouter = express.Router();
const productController = require("../controllers/productController")
const cartController = require("../controllers/cartController")
const { decode, verify, decodeCookiejwt, verifyCookieJWT } = require("../middleware/auth")


//Add to Cart then Show Cart (for order now button)

cartRouter.get("/product/:productId/order", async (req, res) =>{
	const userProfile = decodeCookiejwt(req.cookies.jwt);
	const product = await productController.findProduct(req.params);
	await cartController.getAllUserCartItems(userProfile.id).then ((cartItems) =>{
		/*res.render("cartScreen", { cartItems , product })*/
		/*console.log(product)*/
		console.log(cartItems)		
	})	
})



cartRouter.get("/user/cart/show", (req, res) =>{ //Show user cart
	const userProfile = decodeCookiejwt(req.cookies.jwt)
	cartController.getAllUserCartItems(userProfile.id).then ((cartItems) =>{
		res.render("cartScreen", { cartItems })		
	})
})

//Add to cart - cart button
cartRouter.post("/product/:productId/cart/add", (req, res) =>{// add specific product to cart
	const userProfile = decodeCookiejwt(req.cookies.jwt)
	cartController.addToCart(req.params, userProfile.id).then ((resultFromAddToCart) =>{
	res.redirect("/api/home")
	})
})

cartRouter.get("/user/wishlist/show", verifyCookieJWT, (req, res) =>{ //Show user wishlist
	const userProfile = decodeCookiejwt(req.cookies.jwt)
	cartController.getAllUserWishlist(userProfile.id).then ((resultFromShowWishlist) =>{
		res.send(resultFromShowWishlist)
	})
})

cartRouter.post("/product/:productId/wishlist/add", verifyCookieJWT, (req, res) =>{ //add specific product to wishlist
	const userProfile = decodeCookiejwt(req.cookies.jwt);
	cartController.addToWishlist(req.params, req.body , userProfile.id).then ((resultFromAddToCart) =>{
		res.redirect("/api/home")
	})
})

module.exports = cartRouter;