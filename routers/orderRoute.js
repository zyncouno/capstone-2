const express = require("express");
const mongoose = require("mongoose")
const orderRouter = express.Router();
const orderController = require("../controllers/orderController")
const { decode,verify, decodeCookiejwt, verifyCookieJWT } = require("../middleware/auth")
const { isAdmin, isSeller } = require("../middleware/middleware")



orderRouter.get("/user/order/showAllOrders", verifyCookieJWT, isAdmin, (req, res) =>{
	const userProfile = decodeCookiejwt(req.cookies.jwt)
	orderController.adminShowAllOrders(userProfile.id).then((resFromAdminShowOrders) =>{
	res.send(resFromAdminShowOrders);
	})
})

orderRouter.get("/user/order/findUserOrder", verifyCookieJWT, (req, res) =>{
	const userProfile = decodeCookiejwt(req.cookies.jwt)
	orderController.getAllUserOrders(userProfile.id).then ((resultFromGetuserOrder) =>{
		res.send(resultFromGetuserOrder)
	})
})

orderRouter.post("/order/add", verifyCookieJWT, (req, res) =>{
	const userProfile = decodeCookiejwt(req.cookies.jwt)
	orderController.addToOrder(req.body, userProfile.id).then((resultFromAddToOrder) =>{
		res.send(resultFromAddToOrder)
	})
})

orderRouter.post("/order/:orderId", verifyCookieJWT, isAdmin, (req, res ) => {
	const userProfile = decodeCookiejwt(req.cookies.jwt)
	orderController.changeStatus(req.body, req.params).then((resultFromAddToOrder) =>{
		res.send(resultFromAddToOrder)
	})
})

module.exports = orderRouter;

