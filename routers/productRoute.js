const express = require("express");
const mongoose = require("mongoose")

const productRouter = express.Router();
const productController = require("../controllers/productController")
const { createAccessToken, decodeCookiejwt, verifyCookieJWT } = require("../middleware/auth")
const { isAdmin, isSeller, isAdminOrSeller } = require("../middleware/middleware")

//Home EndPoint

productRouter.get("/home",(req,res) =>{
	productController.renderProducts().then((products)=>{
		res.render("home.ejs", {products})
		console.log(products)
	})
})

productRouter.get("/product/inactive", verifyCookieJWT, isAdmin, (req,res) =>{  // find all inActive products admin
	productController.findInActive().then((resultFromFindActive) =>{
		res.send(resultFromFindActive)
	})
}) 

productRouter.post("/product/:productId/toggle", verifyCookieJWT, isAdmin, (req,res) =>{  // toggle active product true or false
	productController.toggleProduct(req.params).then((resultFromFindActive) =>{
		res.send(resultFromFindActive)
	})
}) 

productRouter.get("/allProducts", (req,res) =>{ // find all active products
	productController.findActive().then((resultFromFindActive) =>{
		res.send(resultFromFindActive)
	})
})

//findProduct
productRouter.get("/product/:productId", (req, res) =>{ //find specific product
	productController.findProduct(req.params).then((product) =>{
		res.render("productView.ejs", { product })
	})
})

//Create Product
productRouter.get("/addproduct", (req, res)=>{
		res.render("addProduct.ejs")
})
productRouter.post("/addproduct", isAdmin, (req, res) =>{ //create product. admin and seller
	const userProfile = decodeCookiejwt(req.cookies.jwt)
	productController.createProduct(req.body, userProfile).then((resFromCreateProduct) =>{
	res.redirect("/api/home")
	})
})

//editproducts
/*productRouter.post("/product/:productId/admin/edit", verifyCookieJWT, isAdmin, (req, res) =>{ // admin/seller edit product
	const userProfile = decodeCookiejwt(req.cookies.jwt)
	productController.editProduct(req.params, req.body, userProfile.id).then((resFromEditproduct)=>{
	res.send(resFromEditproduct)
	})	
})*/

productRouter.post("/product/:productId/edit", verifyCookieJWT, (req, res) =>{ // seller edit product
	const userProfile = decodeCookiejwt(req.cookies.jwt)
	productController.editProduct(req.params, req.body, userProfile.id).then((resFromEditproduct)=>{
	res.send(resFromEditproduct)
	})	
})

productRouter.post("/product/:productId/switch", /*verifyCookieJWT, isAdmin,*/ (req,res) =>{ //toggle user role 
	productController.archiveProduct(req.params, req.body).then((resFromArchiveProd)=>{
	res.send(resFromArchiveProd)
	})	
})

module.exports = productRouter;