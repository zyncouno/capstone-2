const mongoose = require("mongoose");
const { Schema } = mongoose;
const { ObjectId } = mongoose.Schema;


const cartSchema =  new Schema({
	product:{
		type: Array
	},
	price:Number,
	qty: Number,
	cartTotal: Number,
	placedBy:String
},{timestamps: true})

module.exports = mongoose.model("Cart",cartSchema);