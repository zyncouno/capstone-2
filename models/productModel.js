const mongoose = require("mongoose");
const {Schema} = mongoose;
const { ObjectId } = mongoose.Schema;

const productSchema = new Schema({
	name:{
		type: String,
		trim: true,
		maxlength: 200,
		required: ["Product Name is Required"]
	},
	description:{
		type: String,
		required: ["Description is Required"],
		maxlength: 2000,
	},
	image:{
		type: String,
	}, 	
	price:{
		type: Number,
		required: ["Price is Required"],
		trim:true,
		maxlength: 6,
	},
	createdOn:{
    	type: Date,
    	default: new Date(),
  	},
	category:{
		type: String,
		enum: ["Dog food", "Cat Food", "Pet Accessories", "Cages", "Toys"]
	},
	quantity:{
		type: Number,
		sold: Number,
		default: 0
	},
	isActive:{
		type: Boolean,
		default: true
	},
	
	ratings:{
		star: Number,
		postedBy: {type: ObjectId, ref: "User"}
	},
},
{timestamps: true}
)

module.exports = mongoose.model("Product",productSchema);