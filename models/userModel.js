const mongoose = require("mongoose");
const { Schema } = mongoose;
const {ObjectId} = mongoose.Schema;
const bcrypt = require("bcrypt");
const { isEmail } = require("validator");

const userSchema = new Schema({
	firstName:{
		type: String,
		required: ["First Name is Required"],
		trim: true,
		minlength: [2, "Minimum length must be  2 characters"]
	},
	lastName:{
		type: String,
		required: ["Last Name is Required"],
		trim: true,
		minlength: [2, "Minimum length must be  2 characters"]
	},
	email:{
		type: String,
		required: ["Email is Required"],
		trim: true,
		unique: true,
		validate: [isEmail,"Please enter a valid email"]
	},
	cellphoneNum:{
		type: String,
		required: ["Mobile Number is Required"],
		trim: true,
		minlength: [11, "Minimum length must be  11 characters"]
	},
	password:{
		type: String,
		required: true,
		trim: true,
		minlength: [8, "Minimum length must be  8 characters"]
	},
	ntoken:{
		type: [Object],	
	},
	isActive:{
		type: Boolean,
		default: true
	},
	isAdmin:{
		type: Boolean,
		default: false
	},
	isSeller:{
		type: Boolean,
		default: false
	},
	sellerProducts:[
		{
			type: ObjectId,
			ref: "Product"
		}
	],

	cart:[
		{
			type: ObjectId,
			ref: "Product"
		}
	],
	orders:[
		{
			type: ObjectId,
			ref: "Order"
		}
	],
	wishlist:[
		{
			type: ObjectId,
			ref: "Product"
		}
	],
	
})

//hooks

userSchema.post("save", (doc, next)=>{
	console.log("New User has been created & saved to the database", doc)
	next();
})

/*userSchema.pre("save", async function(next){
	this.password = await bcrypt.hash(this.password, 10)
})*/
 
module.exports = mongoose.model("User",userSchema);