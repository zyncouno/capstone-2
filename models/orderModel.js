const mongoose = require("mongoose");
const { Schema } = mongoose;
const { ObjectId } = mongoose.Schema;

const orderSchema =  new Schema({
	items:{
		type: Array,
	},

	paymentIntent:{
		type: String,
		enum: ["COD", "CreditCard", "Gcash", "Paypal"],
		default: "COD"
	},
	orderStatus:{
		type: String,
		default: "Not Processed",
		enum: ["Not Processed","Processing","Dispatched", "Cancelled", "Delivered"]
	},
	orderedBy:{
		type: ObjectId,
		ref: "User"
	},
	total: Number,

},{timestamps: true})

module.exports = mongoose.model("Order",orderSchema);