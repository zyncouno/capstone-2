const express = require("express");
const mongoose = require("mongoose");
const fs = require("fs");
const cookieParser = require("cookie-parser")
require("dotenv").config();

const port =  process.env.PORT || 8000
const app = express();
const {checkUser} = require("./middleware/auth");

app.use(cookieParser());
app.set("view engine", "ejs");
app.use(express.static('public'));
app.use(express.json());
app.use(express.urlencoded({extended: true}));
mongoose.set('useFindAndModify', false);
app.get("*", checkUser);


fs.readdirSync("./routers").map((r) => app.use("/api", require("./routers/"+ r)));

mongoose
  .connect(
    process.env.Database,
    { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex : true }
  )
  .then(() => {
    console.log("DB Connection Stablished");
  })
  .catch((err) => {
    console.log("DB Connection Failed");
    console.log(err);
  });


app.listen(port, () => console.log(`Server Connected on ${port}`));

