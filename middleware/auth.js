const jwt =require("jsonwebtoken");
const secret = "thisismysecret"
const User = require("../models/userModel");


module.exports.createAccessToken = (user) =>{
	const data = {
		id: user._id,
		email: user.email,
		isSeller: user.isSeller,
		isAdmin: user.isAdmin
	}

	const token = jwt.sign(data, secret, {});
	return token
} 



module.exports.verifyCookieJWT = (req, res, next) =>{
	const token = req.cookies.jwt

	if (token) {
		jwt.verify(token, secret, (err, decodedToken) =>{
			if(err){
				console.log(err.message)
				res.redirect('/api/login')
			}else{
				next()
			}
		})
	}else{
		res.redirect('/api/login')
	}
}

//Cookies JWT verification

module.exports.decodeCookiejwt = (token) =>{
		if(token){
			return jwt.verify(token, secret, (err,data) =>{
				if(err){
					return null;
				}else{
					return jwt.decode(token,{complete: true}).payload					
				}
			})

		}else{ //no token
			return null
		}
	}

//JWT Middleware

module.exports.checkUser = (req, res, next) =>{
	const token = req.cookies.jwt
	if (token) {
		jwt.verify(token, secret, async (err, decodedToken) =>{
			if(err){
				console.log(err.message);
				res.locals.user = null;
				next()
			}else{
				let user = await User.findById(decodedToken.id);
				res.locals.user = user;
				next();
			}
		})
	}else{
		console.log(`token authentication success`)
		res.locals.user = null;
		next();
	}
}