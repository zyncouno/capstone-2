const { decodeCookiejwt } = require("../middleware/auth")


module.exports.isAdmin = (req,res,next) =>{
	const userProfile = decodeCookiejwt(req.cookies.jwt)
	if (userProfile.isAdmin) {
    next();
  } else {
    res.status(401).send({ message: 'not an admin' });
  }
}

module.exports.isSeller = (req,res,next) =>{
	const userProfile = decodeCookiejwt(req.cookies.jwt)
	if (userProfile.isSeller) {
    next();
  } else {
    res.status(401).send({ message: 'not an admin' });
  }
}

module.exports.isAdminOrSeller = (req,res,next) =>{
  const userProfile = decodeCookiejwt(req.cookies.jwt)
  console.log(userProfile)
  if (userProfile.isAdmin || userProfile.isSeller) {
    next();
  } else {
    res.status(401).send({ message: 'not an admin' });
  }
}